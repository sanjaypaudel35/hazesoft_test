<?php

namespace Tests;
use App\User;
use App\Company;
use App\Employee;
use App\Department;
use App\CompanyDepartment;
use App\EmployeeDepartments;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

class BaseTestClass extends TestCase
{
    use RefreshDatabase;
    
    protected function create_company($number=1){
        $company=factory(Company::class, $number)->create();
        return $company;
    }
    protected function create_employee($company_id, $number=2){
        $employee=factory(Employee::class,$number)->create(["company_id"=>$company_id]);
        return $employee;
    }
    protected function create_departments($number=2){
        $departments=factory(Department::class,$number)->create();
        return $departments;
    }

    protected function fill_company_department($company_id,$department_id)
    {
        return factory(CompanyDepartment::class)->create([
                "company_id"=>$company_id,
                "department_id"=>$department_id,
            ]);
    }

    protected function fill_employee_department($employee_id,$department_id){
        return factory(EmployeeDepartments::class)->create([
                "employee_id"=>$employee_id,
                "department_id"=>$department_id,
            ]);
    }
    protected function authorized_only(){
        $user = factory(User::class)->create();
        //Testing the protected api, thats need access with authentication
        return Passport::actingAs($user);
    }
}
