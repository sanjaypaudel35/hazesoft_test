<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\BaseTestClass;
use App\Company;

class UnauthorizedAccessTest extends BaseTestClass
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
  public function test_unauthorized_access_to_protected_api(){
   
      //ACTION
      $response0=$this->getJson(route('company.departments',1));
      $response0->assertStatus(401);

      $response1=$this->getJson(route('company.department.employees',[1,1]));
      $response1->assertStatus(401);

      $response2=$this->getJson(route('employee.detail',1));
      $response2->assertStatus(401);
  }
}
