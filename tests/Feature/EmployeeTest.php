<?php

namespace Tests\Feature;

use Tests\BaseTestClass;
use Illuminate\Foundation\Testing\WithFaker;

class EmployeeTest extends BaseTestClass
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_employee_list_in_company(){
        //PREARE DATA
        $company=$this->create_company(1);
        $employee=$this->create_employee($company[0]["id"],2);

        //ACTION
        $response=$this->getJson(route('company.employees',$company[0]["id"]))
        ->assertOk()
        ->json();

        //ASSERTION
        $this->assertEquals(2,count($response['data']));    
    }

    public function test_employee_list_if_company_id_not_matched(){
        //PREARE DATA
        $company=$this->create_company(1);
        $employee=$this->create_employee($company[0]["id"],2);

        //ACTION
        $response=$this->getJson(route('company.employees',44));

        //ASSERTION
        $response->assertStatus(404);    
    }

    public function test_employee_list_in_specific_department_of_company(){
        $this->authorized_only();

        //PREPARE DATA
        $company=$this->create_company(1);
        $department=$this->create_departments(2);

        $this->fill_company_department($company[0]['id'],$department[0]['id']);
        $this->fill_company_department($company[0]['id'],$department[1]['id']);

        $employee=$this->create_employee($company[0]["id"],2);
        $this->fill_employee_department($employee[0]['id'],$department[0]['id']);
        $this->fill_employee_department($employee[1]['id'],$department[0]['id']);

        //ACTION
        $response=$this->getJson(route('company.department.employees',[$company[0]["id"],$department[0]['id']]))
        ->assertOk()
        ->json();

        //ASSERTION
        $this->assertEquals(2,count($response['data'])); 
    }

    public function test_employee_detail(){
        $this->authorized_only();

        //PREPARE DATA
        $company=$this->create_company(1);
        $department=$this->create_departments(2);

        $this->fill_company_department($company[0]['id'],$department[0]['id']);
        $this->fill_company_department($company[0]['id'],$department[1]['id']);

        $employee=$this->create_employee($company[0]["id"],2);
        $this->fill_employee_department($employee[0]['id'],$department[0]['id']);
        $this->fill_employee_department($employee[1]['id'],$department[0]['id']);
          
        //ACTION
        $response=$this->getJson(route('employee.detail',$employee[0]['id']))
        ->assertOk()
        ->json();
        
        //ASSERTION
        $this->assertEquals($employee[0]["name"],$response["data"]["employee_name"]); 
        $this->assertEquals($company[0]["name"],$response["data"]["company"]["name"]);
        $this->assertEquals($department[0]["department_name"],$response["data"]["departments"][0]["department_name"]);
    }
}
