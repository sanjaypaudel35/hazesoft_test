<?php

namespace Tests\Feature;

use Tests\BaseTestClass;
use Illuminate\Support\Collection;
use Illuminate\Foundation\Testing\WithFaker;


class CompanyListTest extends BaseTestClass
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
   
    //NOTE: to run this class test hit : vendor\bin\phpunit tests\Feature\CompanyListTest.php 
   
    public function test_fetch_company_list(){
        //PREPARE DATA
        $companies=$this->create_company(2);

        //ACTION
        $response=$this->getJson(route('companies.list'))
        ->assertOk()
        ->json();

        //ASSERTION
        $this->assertEquals(2,count($response['data']));
  
    }
    public function test_departments_list_of_company(){
        $this->authorized_only();
        //PREPARE DATA
        $department=$this->create_departments(3);
        $company=$this->create_company(1);
        $this->fill_company_department($company[0]['id'],$department[0]['id']);
        $this->fill_company_department($company[0]['id'],$department[1]['id']);
    
        //ACTION
        $response=$this->getJson(route('company.departments',$company[0]['id']))
        ->assertOk()
        ->json();

        //ASSERTION
        $this->assertEquals(2,count($response['data']));
     
    }
}
