<table class="table table-bordered">
        <thead>
        <tr>
            <th>S.N</th>
            <th>Employee Name</th>
            <th>Email</th>
            <th>Contact Number</th>
            <th>Designation</th>
            <th>Company</th>
            <th>Departments</th>
        </tr>
        </thead>
        <tbody>
        @foreach($employees as $key=>$employee)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$employee->name}}</td>
            <td>{{$employee->email}}</td>
            <td>{{$employee->contact}}</td>
            <td>{{$employee->designation}}</td>
            <td>{{$employee->company->name}}</td>
            <td>  <button type="button" class="btn btn-secondary btn-sm" >
                {{count($employee->departments)}}
            </button>
            @foreach($employee->departments as $key=>$department)
            <span class="badge badge-success ml-1">{{$department->department_name}}</span>
            @endforeach</td>
        </tr>
        @endforeach
        
        </tbody>
    </table>