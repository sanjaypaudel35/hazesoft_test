<form action="{{route('admin.employees.store')}}">
    @csrf
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="company_name">Employee Name: @include('layouts.requiredMark')</label>
                <input type="text" class="form-control" id="employee_name" name="name" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="company_contact">Contact Number: @include('layouts.requiredMark')</label>
                <input type="text" class="form-control" id="contact" name="contact" required>
            </div>
        </div>
    </div> 

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="company_contact">Email: @include('layouts.requiredMark')</label>
                <input type="text" class="form-control" id="email" name="email" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="company_contact">Designation: @include('layouts.requiredMark')</label>
                <input type="text" class="form-control" id="designation" name="designation" required>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Assign Employee to company: @include('layouts.requiredMark')</label>
                <select name="company_id"class="form-control" id="company" required>
                <option disabled="disabled" selected="selected">Choose Company</option>
                @foreach($companies as $key=>$company)
                    <option value="{{$company->id}}">{{$company->name}}</option>
                @endforeach
                </select>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label>Select the departments: @include('layouts.requiredMark')<span style="color:red">(Press ctrl to select multiple departments)</span></label>
                <select name="departments_id[]" class="form-control" id="company_departments" required>
                    <option disabled="disabled" selected="selected">Select Departments</option>
                </select>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
</form>
