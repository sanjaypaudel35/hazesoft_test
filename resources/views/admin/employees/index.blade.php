@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2>Add New Employee:</h2>
           @include('admin.employees.partials.create_employee_form')
        </div>
        <div class="col-md-12 mt-5">
            <h2>Employees List:</h2>
           @include('admin.employees.partials.employees_list')
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $("#company").on('change', function() {
        $("#company_departments").html('<option value="null" disabled="disabled">Select departments</option>');
        $("#company_departments").attr("multiple","multiple");
       fetchDepartmentsOfCompany(this.value)
});

function fetchDepartmentsOfCompany(company_id){
    $.ajax({
                url:"{{ route('admin.company.departments') }}",
                method:"get",
                data:{company_id:company_id},
                success:function(data){
                    console.log(data.length);
                    for(var key in data){
                        $("#company_departments").append('<option value="'+data[key]['id']+'">'+data[key]['department_name']+'</option>');
                    }
                    
                },
                error: function (errorThrown) {
                    console.log('error found');
                console.log('Error - ' + errorThrown);
                }
            });
}
    </script>
@endsection
