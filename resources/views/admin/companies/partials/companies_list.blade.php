<table class="table table-bordered">
        <thead>
        <tr>
            <th>S.N</th>
            <th>Company Name</th>
            <th>Company Location</th>
            <th>Contact Number</th>
            <th>no of Department</th>
        </tr>
        </thead>
        <tbody>
        @foreach($companies as $key=>$company)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$company->name}}</td>
            <td>{{$company->location}}</td>
            <td>{{$company->contact}}</td>
            <td>
                <button type="button" class="btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
                {{$company->departments_count}}
            </button>
            @foreach($company->departments as $key=>$department)
            <span class="badge badge-success ml-1">{{$department->department_name}}</span>
            @endforeach
            </td>
        </tr>
        @endforeach
        
        </tbody>
    </table>