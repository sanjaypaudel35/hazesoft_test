<form action="{{route('admin.companies.store')}}">
    @csrf
    <div class="form-group">
        <label for="company_name">Company Name: @include('layouts.requiredMark')</label>
        <input type="text" class="form-control" id="company_name" name="name" required>
    </div>
    <div class="form-group">
        <label for="company_location">Company Location: @include('layouts.requiredMark')</label>
        <input type="text" class="form-control" id="location" name="location" required>
    </div>

    <div class="form-group">
        <label for="company_contact">Contact Number: @include('layouts.requiredMark')</label>
        <input type="text" class="form-control" id="location" name="contact" required>
    </div>
    <div class="form-group">
        <label>Select Departments For Company <span style="color:red">@include('layouts.requiredMark') (Press ctrl to select multiple departments)</span></label>
    <select multiple name="company_departments[]"class="form-control" id="exampleFormControlSelect2">
      <option disabled="disabled">Choose Department</option>
      @foreach($departments as $key=>$department)
        <option value="{{$department->id}}">{{$department->department_name}}</option>
      @endforeach
    </select>
</div>
    
    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
</form>