@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2>Add New Company:</h2>
           @include('admin.companies.partials.create_company_form')
        </div>
        <div class="col-md-12 mt-5">
            <h2>Companies List:</h2>
           @include('admin.companies.partials.companies_list')
        </div>
    </div>
</div>
@endsection
