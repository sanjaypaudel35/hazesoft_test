@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2>Add New Department:</h2>
           @include('admin.departments.partials.create_department_form')
        </div>
        <div class="col-md-12 mt-5">
            <h2>Departments List:</h2>
           @include('admin.departments.partials.departments_list')
        </div>
    </div>
</div>
@endsection
