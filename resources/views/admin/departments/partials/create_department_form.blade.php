<form action="{{route('admin.departments.store')}}">
    @csrf
    <div class="form-group">
        <label for="department_name">Deparment Name: <span style="color:red">*</span></label>
        <input type="text" class="form-control" id="department_name" name="department_name" required>
    </div>
    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
</form>