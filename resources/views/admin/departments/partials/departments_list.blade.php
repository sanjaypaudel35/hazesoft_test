<table class="table table-bordered">
                <thead>
                <tr>
                    <th>S.N</th>
                    <th>Department</th>
                </tr>
                </thead>
                <tbody>
                @foreach($departments as $key=>$department)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$department->department_name}}</td>
                </tr>
                @endforeach
                
                </tbody>
            </table>