<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\EmployeeRequest;
use App\Employee;
use App\Company;

class EmployeeController extends Controller
{
    //
    private $base_path="admin.employees";
    private $base_route="admin.employees";

    public function index(){
        $companies=Company::all();
        $employees=Employee::with("company")->with("departments")->get();
        return view($this->base_path.'.index',compact('employees','companies'));
    }

    public function store(EmployeeRequest $request){
        $employee=Employee::create($request->except("departments_id"));
        $employee->departments()->sync($request->input("departments_id"));
        return redirect()->route($this->base_route.'.index')->with("success","employee is added successfully");
    }
}
