<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\DepartmentRequest;
use App\Department;

class DepartmentController extends Controller
{
    //
    private $base_path="admin.departments";
    private $base_route="admin.departments";

    public function index(){
        $departments=Department::all();
        return view($this->base_path.'.index',compact('departments'));
    }

    public function store(DepartmentRequest $request){
        Department::create($request->all());
        return redirect()->route($this->base_route.'.index')->with('success','Department is added Successfully');
    }
}
