<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;
use Illuminate\Support\Collection;
use App\Company;
use App\Department;

class CompanyController extends Controller
{
    //
    private $base_path="admin.companies";
    private $base_route="admin.companies";

    public function index(){
        $companies=Company::withCount('departments')->with('departments')->get();
        $departments=Department::all();
        return view($this->base_path.'.index',compact('companies','departments'));
    }
    public function store(CompanyRequest $request){
        $company=Company::create($request->all());
        $company->departments()->sync($request->input('company_departments'));
        return redirect()->route($this->base_route.'.index')->with('success','Company is added Successfully');
    }

    public function companyDepartments(Request $request){
        $company_departments=Company::find($request->input('company_id'))->departments;
        return $company_departments;
    }
}
