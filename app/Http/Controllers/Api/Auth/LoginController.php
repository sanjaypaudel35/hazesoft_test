<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Traits\ApiResponser;
use Validator;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    //
    use ApiResponser;
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            $success['name'] =  $user->name;
   
            return $this->successResponse($success, 'User login successfully.');
        } 
        else{ 
            $message=[
                "success"=>"Fail",
                "message"=>"Unauthorized Access",
            ];
            return $this->errorResponse(404,$message);
         
        } 
    }
}
