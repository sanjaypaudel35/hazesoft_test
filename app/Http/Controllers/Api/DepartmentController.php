<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Department as DepartmentResource;
use App\Http\Services\DataExistance;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Company;

class DepartmentController extends Controller
{
    //
    use ApiResponser;

    private $data_existance;

    public function __construct(DataExistance $de){
        //instantiating the DataExistance class, and assiging its object to private variable
        $this->data_existance=$de;
    }
   
    public function departmentsCompany($company_id){
        $company_exist=$this->data_existance->checkCompanyExist($company_id);
        if($company_exist["status"]==false)
            return $this->errorResponse('404', "Company with this id doesnot exist");

        $company=Company::find($company_id);
        $departments=$company->departments;
        return DepartmentResource::collection($departments);
    }
}
