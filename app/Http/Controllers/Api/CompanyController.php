<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Company as CompanyResource;
use App\Http\Resources\CompanyCollection;
use App\Http\Resources\Department as DepartmentResource;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Company;

class CompanyController extends Controller
{
    //
    use ApiResponser;
    
    public function index(){
        $companies=Company::all();
        $paginate=request()->paginate;

        if($paginate>0)
            $companies=Company::paginate($paginate);

        $companies=$this->filterData($companies);
        return new CompanyCollection($companies);
        // return CompanyResource::collection($companies);
    }


   
}
