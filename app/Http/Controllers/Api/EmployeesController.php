<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Employees as EmployeesResource;
use App\Http\Resources\Employee as EmployeeResource;
use App\Http\Resources\Department as DepartmentResource;
use App\Http\Services\DataExistance;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Department;
use App\Company;
use App\Employee;

class EmployeesController extends Controller
{
    //
    use ApiResponser;

    private $data_existance;

    public function __construct(DataExistance $de){
        //instantiating the DataExistance class, and assiging its object to private variable
        $this->data_existance=$de;
    }
    
    public function employeesCompany($company_id){
        //check if company with this id exist or not
        $company_exist=$this->data_existance->checkCompanyExist($company_id);
        if($company_exist["status"]==false)
            return $this->errorResponse('404', "Company with this id doesnot exist");

        $company=Company::find($company_id);
        //fetching employees belonging to this company using relationship
        $employees=$company->employees;
        //accessing query params 'paginate'
        $paginate=request()->paginate;
        if($paginate>0)
            $employees=$company->employees()->paginate($paginate);

        $employees=$this->filterData($employees);
        return EmployeesResource::collection($employees);
    }

    public function employeesInCompanyDepartment($company_id, $department_id){
        //checking if company with this id exist or not
        $company_exist=$this->data_existance->checkCompanyExist($company_id);
        //checking if department with this id exist or not
        $department_exist=$this->data_existance->checkDepartmentExist($department_id);
        
        if($company_exist['status']==true && $department_exist['status']==true){
            $department=Department::find($department_id);
            $employees=$department->employees;
            $employees=$employees->where("company_id",$company_id);
            $employees=$this->filterData($employees);
            return EmployeesResource::collection($employees);
        }else{
            $message=$company_exist["message"]." ".$department_exist["message"];
            return $this->errorResponse(404,$message);
        }    
    }

    public function Detail($employee_id){
        //checking if employee with this id exist or not
        $employee_exist=$this->data_existance->checkEmployeeExist($employee_id);

        if($employee_exist['status']==false)
            return $this->errorResponse('404', $employee_exist["message"]);

        $employee=Employee::find($employee_id);
        return new EmployeeResource($employee);
    }
}
