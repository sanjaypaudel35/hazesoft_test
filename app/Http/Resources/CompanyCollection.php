<?php

namespace App\Http\Resources;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */


    public $collects = 'App\Http\Resources\Company'; 


    public function toArray($request)
    {
        // return parent::toArray($request);
       
        $queryToAdd=null;
        $links=null;
        if(request()->has('paginate')){
            $queryToAdd='?paginate='.request()->paginate;
            $links=[
                'base_url'=>$request->url().$queryToAdd,
                'note'=>'for pagination data do not forget to pass paginate query params on next and prev url along with page number',
            ];
        }

        return [
            'data' => $this->collection,
            'links' => $links,  
        ];
    }
}
