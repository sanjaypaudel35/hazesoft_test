<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Company extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return[
            'id'=>$this->id,
            'name'=>$this->name,
            'location'=>$this->location,
            'contact'=>$this->contact,
            'no_of_departments'=>$this->departments->count(),
            'no_of_emmployees'=>$this->employees->count(),
        ];
    }
}
