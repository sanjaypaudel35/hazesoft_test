<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Employees extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return[
            'id'=>$this->id,
            'name'=>$this->name,
            'contact'=>$this->contact,
            'email'=>$this->email,
            'designation'=>$this->designation,
            'company_id'=>$this->company_id,
            'company_name'=>$this->company->name,     
        ];
    }
}
