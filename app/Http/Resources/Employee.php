<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Department as DepartmentResource;
use App\Http\Resources\Company as CompanyResource;

class Employee extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return[
            'id'=>$this->id,
            'employee_name'=>$this->name,
            'employee_contact'=>$this->contact,
            'employee_email'=>$this->email,
            'designation'=>$this->designation,
            'company_id'=>$this->company_id,
            'company'=>new CompanyResource($this->company),
            'departments'=>DepartmentResource::collection($this->departments),
          
        ];
    }
}
