<?php

namespace App\Http\Services;
use App\Company;
use App\Department;
use App\Employee;

class DataExistance
{
    private function dataFoundResponse(){
        return ["status"=>true,"message"=>null];
    }
    private function dataNotFoundResponse($message){
        return ["status"=>false, "message"=>$message];
    }

    public function checkCompanyExist($company_id){
        if(!Company::find($company_id))
            return $this->dataNotFoundResponse("Company with this id not found");
        else 
            return $this->dataFoundResponse();
    }

    public function checkDepartmentExist($department_id){
        if(!Department::find($department_id))
          return $this->dataNotFoundResponse("Department with this id not found");
        else
            return $this->dataFoundResponse();
    }

    public function checkEmployeeExist($employee_id){
        if(!Employee::find($employee_id))
            return $this->dataNotFoundResponse("Employee with this id not found");
        else
            return $this->dataFoundResponse();
    }
}