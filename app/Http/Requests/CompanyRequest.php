<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name'=>'required|string',
            'location'=>'required|string',
            'contact'=>'required|numeric|min:10',
            'company_departments'=>'required',
        ];
    }

    public function messages()
        {
            return [
                'company_departments.required' => 'Required to have at least one department for the company',
            ];
        }
}
