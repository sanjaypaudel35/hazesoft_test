<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name'=>'required|string',
            'contact'=>'required|string|numeric|min:10',
            'email'=>'required|email|unique:employees',
            'designation'=>'required',
            'company_id'=>'required',
            'departments_id'=>'required',
        ];
    }

    
    public function messages()
        {
            return [
                'departments_id.required' => 'Required to assign employee to atleast one department',
            ];
        }
}
