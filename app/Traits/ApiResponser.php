<?php
namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;


trait ApiResponser{
 
    //filtering data based on query params (name,value) pair
    protected function filterData($collection){
        if(request()->except('paginate')) {
            foreach (request()->query() as $query => $value) {
                $attribute = $query;
                if (isset($attribute, $value) && $attribute!='paginate' && $attribute!='page') {
                    //strictly matching value
                    $collection = $collection->where($attribute, $value);
                }
            }
        }
        return $collection;
    }

    protected function errorResponse($code=404, $message=null){
        return response()->json(["status_code"=>$code, "message"=>$message],$code);
    }
    protected function successResponse($result,$message){
        $response = [
            'success' => true,
            'message' => $message,
            'data'    => $result,      
        ];
        return response()->json($response, 200);
    }

 

}