<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Company extends Model
{
    //

    protected $table="companies";
    public $timestamps = false;
    protected $fillable=[
        "name",
        "location",
        "contact",
    ];


    public function departments()
    {
        return $this->belongsToMany('App\Department','company_departments','company_id','department_id');
    }

    public function employees()
    {
        return $this->hasMany('App\Employee','company_id');
    }
}
