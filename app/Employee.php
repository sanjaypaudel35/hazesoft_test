<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
    protected $table="employees";

    protected $guarded=[];

    public function departments()
    {
        return $this->belongsToMany('App\Department','employee_departments','employee_id','department_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Company','company_id');
    }
}
