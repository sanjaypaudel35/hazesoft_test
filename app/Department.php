<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Employee;

class Department extends Model
{
    //
    protected $fillable=['department_name'];

    public function companies()
    {
        return $this->belongsToMany('App\Company','company_departments','department_id','company_id');
    }
    public function employees()
    {
        return $this->belongsToMany('App\Employee','employee_departments','department_id','employee_id');
    }
}
