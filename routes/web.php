<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');

Auth::routes();
Route::group(['middleware' => 'web_auth','prefix' => 'admin','as'=>'admin.'], function () {
//DEPARTMENT
Route::get('', 'HomeController@index')->name('home');
Route::get('departments','Admin\DepartmentController@index')->name('departments.index');
Route::get('departments/store','Admin\DepartmentController@store')->name('departments.store');

//COMPANY
Route::get('companies','Admin\CompanyController@index')->name('companies.index');
Route::get('companies/store','Admin\CompanyController@store')->name('companies.store');
//when company id is passed it gives all the departments belongs to that company.
Route::get('company/departments','Admin\CompanyController@companyDepartments')->name('company.departments');

//EMPLOYEE
Route::get('employees','Admin\EmployeeController@index')->name('employees.index');
Route::get('employees/store','Admin\EmployeeController@store')->name('employees.store');
});


