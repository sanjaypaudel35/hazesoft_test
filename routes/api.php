<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('login', 'Api\Auth\LoginController@login');
//gives list of companies
Route::get('companies','Api\CompanyController@index')->name('companies.list');
//gives employees list of specific company
Route::get('employees/{company_id}','Api\EmployeesController@employeesCompany')->name('company.employees');
 

Route::middleware('auth:api')->group( function () {
   //gives departments of specific company
   Route::get('departments/{company_id}','Api\DepartmentController@departmentsCompany')->name('company.departments');
    //gives list employees in a specific department of a company.
    Route::get('employees/{company_id}/{department_id}','Api\EmployeesController@employeesInCompanyDepartment')->name('company.department.employees');
    //gives employee detail with respective company and departments
    Route::get('employee/detail/{employee_id}','Api\EmployeesController@Detail')->name('employee.detail');
});




