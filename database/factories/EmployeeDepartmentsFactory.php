<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\EmployeeDepartments;
use Faker\Generator as Faker;
use App\Employee;
use App\Department;

$factory->define(EmployeeDepartments::class, function (Faker $faker) {
    return [
        //
        'employee_id'=>factory(Employee::class)->create()->id,
        'department_id'=>factory(Department::class)->create()->id,
    ];
});
