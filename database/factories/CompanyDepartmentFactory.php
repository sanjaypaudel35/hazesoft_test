<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CompanyDepartment;
use Faker\Generator as Faker;
use App\Company;
use App\Department;

$factory->define(CompanyDepartment::class, function (Faker $faker) {
    return [
        'company_id'=>factory(Company::class)->create()->id,
        'department_id'=>factory(Department::class)->create()->id,
    ];
});
