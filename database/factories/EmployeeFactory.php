<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Employee;
use App\Company;
use Faker\Generator as Faker;

$factory->define(Employee::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->name,
        'email' => $faker->email,
        'contact'=>$faker->phoneNumber,
        'designation'=>$faker->name,
        'company_id'=>factory(Company::class)->create()->id,
    ];
});
